cmake_minimum_required(VERSION 3.22)

SET(PROJECT_NAME "CHAT_APP")
project(${PROJECT_NAME} VERSION 1.0.0 LANGUAGES CXX)

find_package(main REQUIRED PATHS ${CMAKE_CONFIG_SEARCH_DIR}) 

set(SOURCES
    main.cpp
)

chat_app_add_executable(NAME         ${PROJECT_NAME} 
                        SOURCES      ${SOURCES}
                        LIBRARIES    ChatAppBll ChatAppUil
                        )   