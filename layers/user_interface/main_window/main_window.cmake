cmake_minimum_required(VERSION 3.22)

set(LIB_NAME "main_window")
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 REQUIRED COMPONENTS Widgets)
include_directories(${Qt6Widgets_INCLUDE_DIRS})
add_definitions(${Qt6Widgets_DEFINITIONS})

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(SRC_DIR
    ${CMAKE_CURRENT_LIST_DIR}/src/main_window.ui
    ${CMAKE_CURRENT_LIST_DIR}/src/main_window.cpp
    ${CMAKE_CURRENT_LIST_DIR}/include/main_window.hpp
   )

set(INCLUDE_DIR
    ${CMAKE_CURRENT_LIST_DIR}/include
   )

set(QT_LIBS
    Qt6::Widgets
)

add_library(${LIB_NAME} STATIC)
target_sources(${LIB_NAME} PUBLIC ${SRC_DIR})
target_include_directories(${LIB_NAME} PUBLIC ${INCLUDE_DIR})
target_link_libraries(${LIB_NAME} PRIVATE ${QT_LIBS})
target_link_libraries(${LIB_NAME} PRIVATE chat_window)
add_dependencies(${LIB_NAME} chat_window) 
target_link_libraries(${LIB_NAME} PRIVATE ChatAppBll)
add_dependencies(${LIB_NAME} ChatAppBll) 
set_target_properties(${LIB_NAME} PROPERTIES
    WIN32_EXECUTABLE ON
    MACOSX_BUNDLE ON
)
target_link_libraries(ChatAppUil INTERFACE ${LIB_NAME})