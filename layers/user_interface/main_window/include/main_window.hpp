#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <QMainWindow>
#include "chat_app_facade.hpp"

using namespace chat_app;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void handleServerCheckBoxToggled(bool checked);
    void handleClientCheckBoxToggled(bool checked);
    void handleStartButtonClicked();

private:
    Ui::MainWindow *ui;
    void updateUI();
};
#endif // MAIN_WINDOW_HPP
