#include "main_window.hpp"
#include "chat_window.hpp"
#include "ui_main_window.h"
#include <QMessageBox>
#include <string>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->startButton->setEnabled(false);

    connect(ui->serverCheckBox, &QCheckBox::toggled, this, &MainWindow::handleServerCheckBoxToggled);
    connect(ui->clientCheckBox, &QCheckBox::toggled, this, &MainWindow::handleClientCheckBoxToggled);
    connect(ui->startButton, &QPushButton::clicked, this, &MainWindow::handleStartButtonClicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleServerCheckBoxToggled(bool checked)
{
    if (checked) {
        ui->clientCheckBox->setChecked(false);
        ui->serverIp->setReadOnly(true);
    } else {
        ui->serverIp->setReadOnly(false);
    }
    updateUI();
}

void MainWindow::handleClientCheckBoxToggled(bool checked)
{
    if (checked) {
        ui->serverCheckBox->setChecked(false);
    }
    updateUI();
}

void MainWindow::updateUI()
{
    bool startButtonEnabled = ui->serverCheckBox->isChecked() || ui->clientCheckBox->isChecked();
    ui->startButton->setEnabled(startButtonEnabled);
}

void MainWindow::handleStartButtonClicked()
{
    QString serverIp = ui->serverIp->text();
    int port = ui->port->text().toInt();

    std::string serverIpStdString = serverIp.toStdString();

    ChatAppType appType;
    if (ui->serverCheckBox->isChecked()) {
        appType = ChatAppType::Server;
    } else if (ui->clientCheckBox->isChecked()) {
        appType = ChatAppType::Client;
    } else {
        return;
        QMessageBox::warning(this, "Warning", "Please select either Server or Client mode.");
    }
    ChatWindow *chatWindow = new ChatWindow(appType, port, serverIpStdString);
    chatWindow->show();
    this->close();
}
