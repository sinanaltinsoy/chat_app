#ifndef CONN_WORKER_HPP
#define CONN_WORKER_HPP

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include "chat_app_facade.hpp"

using namespace chat_app;

class ConnWorker : public QThread
{
    Q_OBJECT
public:
    explicit ConnWorker(ChatAppFacade *chatApp, QWaitCondition *connCondition, QMutex *connMutex, QObject *parent = nullptr)
        : QThread(parent), chatApp(chatApp), connCondition(connCondition), connMutex(connMutex)
    {
    }

protected:
    void run() override
    {
        while (true) {
            QMutexLocker locker(connMutex);
            connCondition->wait(connMutex); 
            chatApp->connectApp();
        }
    }

private:
    ChatAppFacade *chatApp;
    QWaitCondition *connCondition;
    QMutex *connMutex;
};

#endif // CONN_WORKER_HPP
