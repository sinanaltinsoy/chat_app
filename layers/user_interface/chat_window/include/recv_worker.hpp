#ifndef RECV_WORKER_HPP
#define RECV_WORKER_HPP

#include <QObject>
#include <QThread>
#include <QDebug>
#include "chat_app_facade.hpp"

class RecvWorker : public QObject
{
    Q_OBJECT

public:
    RecvWorker(ChatAppFacade* chatApp, QObject* parent = nullptr)
        : QObject(parent), chatApp(chatApp) {}

public slots:
    void receiver()
    {
        std::string receivedMessage;
        while (true) {
            if (chatApp->recvMsg(receivedMessage) == ChatAppError::None) {
                emit messageReceived(QString::fromStdString(receivedMessage));
            }
            QThread::msleep(250);
        }
    }

signals:
    void messageReceived(const QString& message);

private:
    ChatAppFacade* chatApp;
};

#endif // RECV_WORKER_HPP