#ifndef CHAT_WINDOW_HPP
#define CHAT_WINDOW_HPP

#include <QWidget>
#include <QPushButton>
#include <QTextEdit>
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QObject>
#include <QTextEdit>
#include "chat_app_facade.hpp"
#include "conn_worker.hpp"
#include "recv_worker.hpp"

using namespace chat_app;

QT_BEGIN_NAMESPACE
namespace Ui {
class ChatWindow;
}
QT_END_NAMESPACE

class ChatWindow : public QWidget {
    Q_OBJECT

public:
    ChatWindow(ChatAppType appType, int port, std::string serverIp, QWidget *parent = nullptr);
    ~ChatWindow();

public slots:
    void startApp();
    void stopApp();
    void connectApp();
    void sendMessage();
    void recvMessage(const QString& message);
    void updateStatus();
    
private:
    ChatAppFacade *chatApp;
    Ui::ChatWindow *ui;
    QTimer *statusTimer;
    QWaitCondition *connCondition;
    QMutex *connMutex;
    ConnWorker *connWorker;
    QWaitCondition *recvCondition;
    QMutex *recvMutex;
    RecvWorker *recvWorker;
    QThread *recvThread;
};

#endif // CHAT_WINDOW_HPP
