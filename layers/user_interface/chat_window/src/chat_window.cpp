#include "chat_window.hpp"
#include "ui_chat_window.h"
#include <QTimer>
#include <QDebug>

ChatWindow::ChatWindow(chat_app::ChatAppType appType, int port, std::string serverIp, QWidget *parent) 
    :   QWidget(parent),
        ui(new Ui::ChatWindow),
        chatApp(nullptr),
        statusTimer(nullptr),
        connCondition(nullptr), 
        connMutex(nullptr), 
        connWorker(nullptr),
        recvWorker(nullptr),
        recvThread(nullptr)  {

    qDebug() << "ChatWindow constructor started";
    chatApp = new ChatAppFacade(appType, port, serverIp);
    ui->setupUi(this);
    ui->sessionOutput->setTextColor(Qt::black);
    connect(ui->startConnButton, &QPushButton::clicked, this, &ChatWindow::connectApp);
    connect(ui->stopConnButton, &QPushButton::clicked, this, &ChatWindow::stopApp);
    connect(ui->sendMsgButton, &QPushButton::clicked, this, &ChatWindow::sendMessage);

    if(chatApp->getAppType() == ChatAppType::Server) {
        ui->appTypeOutLabel->setText(QString::fromStdString("Server"));
        ui->serverIpOutLabel->setText(QString::fromStdString(""));
    }
    else {
        ui->appTypeOutLabel->setText(QString::fromStdString("Client"));
        ui->serverIpOutLabel->setText(QString::fromStdString(chatApp->getIpAddr()));
    }

    ui->portOutLabel->setText(QString::number(chatApp->getPortNo()));

    startApp();
    updateStatus();

    statusTimer = new QTimer(this);
    connect(statusTimer, &QTimer::timeout, this, &ChatWindow::updateStatus);
    statusTimer->start(1000);

    recvWorker = new RecvWorker(chatApp);
    recvThread = new QThread();
    recvWorker->moveToThread(recvThread);

    connect(recvThread, &QThread::started, recvWorker, &RecvWorker::receiver);
    connect(recvWorker, &RecvWorker::messageReceived, this, &ChatWindow::recvMessage);

    recvThread->start();

    connMutex = new QMutex();
    connCondition = new QWaitCondition();
    connWorker = new ConnWorker(chatApp, connCondition, connMutex);
    connWorker->start(); 

    qDebug() << "ChatWindow constructor finished";
}

ChatWindow::~ChatWindow() {
    qDebug() << "ChatWindow destructor started";
    delete chatApp;
    delete ui;
    delete connMutex;
    delete connCondition;
    delete connWorker;
    delete recvMutex;
    delete recvCondition;
    delete recvWorker;
    qDebug() << "ChatWindow destructor finished";
}

void ChatWindow::startApp() {
    qDebug() << "startApp method called";
    if (chatApp->startApp() == ChatAppError::None) {
        updateStatus();
    }
}

void ChatWindow::stopApp() {
    qDebug() << "stopApp method called";
    if (chatApp->stopApp() == ChatAppError::None) {
        updateStatus();
    }
}

void ChatWindow::connectApp() {
    qDebug() << "connectApp method called";
    ChatAppStatus status = chatApp->getAppStatus();
    if(status == ChatAppStatus::Offline) {
        chatApp->startApp();
    } 
    else if(status == ChatAppStatus::Connected) {
        return; // app is connected, no need to connect again
    }

    if (chatApp->getAppStatus() == ChatAppStatus::Online) {
        {
            QMutexLocker locker(connMutex);
            connCondition->wakeAll();
        }
    }

    updateStatus();
}

void ChatWindow::sendMessage() {
    qDebug() << "sendMessage method called";
    ChatAppStatus status = chatApp->getAppStatus();
    if (status == ChatAppStatus::Connected) {
        std::string message = ui->msgInputLine->text().toStdString();
        if (chatApp->sendMsg(message) == ChatAppError::None) {
            ui->sessionOutput->append("Sent: " + QString::fromStdString(message));
        }
    }
}

void ChatWindow::recvMessage(const QString& message) {
    ui->sessionOutput->append("Received: " + message);
}

void ChatWindow::updateStatus() {
    qDebug() << "updateStatus method called";
    ChatAppStatus status = chatApp->getAppStatus();
    if(status == ChatAppStatus::Connected) {
        qDebug() << "App is Connected";
        ui->statusOutLabel->setText(QString::fromStdString("Connected"));
        ui->startConnButton->setEnabled(false); 
        ui->stopConnButton->setEnabled(true);
    }
    else if(status == ChatAppStatus::Online) {
        qDebug() << "App is Online";
        ui->statusOutLabel->setText(QString::fromStdString("Online"));
        ui->startConnButton->setEnabled(true); 
        ui->stopConnButton->setEnabled(true);        
    }
    else if(status == ChatAppStatus::Offline) {
        qDebug() << "App is Offline";
        ui->statusOutLabel->setText(QString::fromStdString("Offline"));
        ui->startConnButton->setEnabled(true); 
        ui->stopConnButton->setEnabled(false);         
    }
    else {
        qDebug() << "App is InvalidType";
    }
}

