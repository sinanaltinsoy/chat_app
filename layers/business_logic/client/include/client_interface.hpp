#ifndef CLIENT_INTERFACE_HPP
#define CLIENT_INTERFACE_HPP

#include <string>
#include "client_types.hpp"

namespace chat_app {

class IClient {
public:
    virtual ClientError startClient() = 0;
    virtual ClientError stopClient() = 0;
    virtual ClientError connectToServer() = 0;
    virtual ClientError sendMsg() = 0;
    virtual ClientError recvMsg() = 0;
    virtual ClientError sendMsgToQueue(const std::string &aMessage) = 0;
    virtual ClientError recvMsgFromQueue(std::string &aMessage) = 0;
    virtual void setStatus(ClientStatus aStatus) = 0;
    virtual ClientStatus getStatus() const = 0;
    virtual int getSocket() const = 0;
    virtual void notifyServer() = 0;
    virtual void startThreads() = 0;
    virtual void stopThreads() = 0;
    virtual void recvThreadFunc() = 0;
    virtual void sendThreadFunc() = 0;
    virtual ~IClient() = default;
};

} // namespace chat_app

#endif // CLIENT_INTERFACE_HPP
