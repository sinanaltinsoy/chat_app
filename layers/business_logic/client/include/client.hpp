#ifndef CLIENT_HPP
#define CLIENT_HPP

#include "client_interface.hpp"
#include "client_types.hpp"
#include <string>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <queue>
#include <mutex>

namespace chat_app {

class Client : public IClient {
public:
    Client(const std::string &aServerIp, int aPortNo);
    ~Client();

    ClientError startClient() override;
    ClientError stopClient() override;
    ClientError connectToServer() override;
    ClientError sendMsg() override;
    ClientError recvMsg() override;
    ClientError sendMsgToQueue(const std::string &aMessage) override;
    ClientError recvMsgFromQueue(std::string &aMessage) override;
    void setStatus(ClientStatus aStatus) override;
    ClientStatus getStatus() const override;
    int getSocket() const override;
    void notifyServer() override;
    void startThreads() override;
    void stopThreads() override;
    void recvThreadFunc() override;
    void sendThreadFunc() override;

private:
    int mServerSocket;
    ClientStatus mClientStatus;
    std::string mServerIp;
    int mPortNo;

    std::mutex mRcvMsgMutex;
    std::mutex mSendMsgMutex;
    std::queue<std::string> mRecvMsgQueue;
    std::queue<std::string> mSendMsgQueue;

    std::condition_variable mRecvMsgCondVar;
    std::condition_variable mSendMsgCondVar;
    std::thread mRecvThread;
    std::thread mSendThread;
    std::atomic<bool> mStopThreads{false};
};

} // namespace chat_app

#endif // CLIENT_HPP
