#ifndef CLIENT_TYPES_HPP
#define CLIENT_TYPES_HPP

#include <string>

namespace chat_app {

    enum class ClientStatus : unsigned char {
        Online,
        Offline,
        Connected,
    };

    enum class ClientError : unsigned char {
        None = 0,
        NotConnected,
        InvalidType,
        Failed
    };

} // namespace chat_app

#endif // CLIENT_TYPES_HPP
