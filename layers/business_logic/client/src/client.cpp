#include "client.hpp"
#include <iostream>
#include <unistd.h>
#include <netinet/in.h>
#include <cstring>
#include <sys/socket.h>
#include <arpa/inet.h>

namespace chat_app {

Client::Client(const std::string &aServerIp, int aPortNo) : 
    mServerSocket(-1), 
    mClientStatus(ClientStatus::Offline),
    mServerIp(aServerIp),
    mPortNo(aPortNo) {
    startThreads();
}

Client::~Client() {
    if (mClientStatus == ClientStatus::Connected) {
        stopClient();
    }
    stopThreads();
}

ClientError Client::startClient() {
    mServerSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (mServerSocket == -1) {
        std::cerr << "Failed to create socket" << std::endl;
        return ClientError::Failed;
    }
    setStatus(ClientStatus::Online);
    return ClientError::None;
}

ClientError Client::stopClient() {
    if (mClientStatus == ClientStatus::Connected) {
        setStatus(ClientStatus::Offline);
        std::this_thread::sleep_for(std::chrono::seconds(3));
        close(mServerSocket);
    }
    return ClientError::None;
}

ClientError Client::connectToServer() {
    sockaddr_in serverAddr;
    std::memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(mServerIp.c_str());
    serverAddr.sin_port = htons(mPortNo);

    if (connect(mServerSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
        std::cerr << "Failed to connect to server" << std::endl;
        return ClientError::Failed;
    }

    setStatus(ClientStatus::Connected);

    std::cout << "Connected to server " << mServerIp << ":" << mPortNo << std::endl;
    return ClientError::None;
}

ClientError Client::sendMsg() {
    std::string aMessage;
    {
        std::lock_guard<std::mutex> lock(mSendMsgMutex);
        if (mSendMsgQueue.empty()) {
            return ClientError::Failed;
        }
        aMessage = mSendMsgQueue.front();
        mSendMsgQueue.pop();
    }

    if (mClientStatus == ClientStatus::Connected) {
        int retVal = send(mServerSocket, aMessage.c_str(), aMessage.length(), 0);
        if (retVal > 0) {
            std::cout << "Sent message: " << aMessage << std::endl;
            return ClientError::None;
        }
    }
    std::cerr << "Failed to send message" << std::endl;
    return ClientError::Failed;
}

ClientError Client::recvMsg() {
    if (mClientStatus == ClientStatus::Connected) {
        char buffer[1024];
        ssize_t bytesRead = recv(mServerSocket, buffer, sizeof(buffer) - 1, 0);
        if (bytesRead > 0) {
            buffer[bytesRead] = '\0';
            std::string message(buffer);
            {
                std::lock_guard<std::mutex> lock(mRcvMsgMutex);
                mRecvMsgQueue.push(message);
            }
            std::cout << "Received message: " << message << std::endl;
            mRecvMsgCondVar.notify_one();

            // Check for connection status change messages
            if (message == "Server is Offline") {
                setStatus(ClientStatus::Online);
            } else if (message == "Server is Online") {
                setStatus(ClientStatus::Online);
            }

            return ClientError::None;
        }
    }
    std::cerr << "Failed to receive message" << std::endl;
    setStatus(ClientStatus::Offline);
    return ClientError::Failed;
}

ClientError Client::sendMsgToQueue(const std::string &aMessage) {
    {
        std::lock_guard<std::mutex> lock(mSendMsgMutex);
        mSendMsgQueue.push(aMessage);
    }
    mSendMsgCondVar.notify_one();
    return ClientError::None;
}

ClientError Client::recvMsgFromQueue(std::string &aMessage) {
    std::lock_guard<std::mutex> lock(mRcvMsgMutex);
    if (!mRecvMsgQueue.empty()) {
        aMessage = mRecvMsgQueue.front();
        mRecvMsgQueue.pop();
        return ClientError::None;
    }
    return ClientError::Failed;
}

void Client::setStatus(ClientStatus status) {
    mClientStatus = status;
    notifyServer();
}

int Client::getSocket() const {
    return mServerSocket;
}

ClientStatus Client::getStatus() const {
    return mClientStatus;
}

void Client::notifyServer() {
    std::string statusMessage = "Client is ";
    if(mClientStatus == ClientStatus::Online) statusMessage += "Online";
    else if (mClientStatus == ClientStatus::Offline) statusMessage += "Offline";
    else if (mClientStatus == ClientStatus::Connected) statusMessage += "Connected";
    statusMessage += "\n";

    sendMsgToQueue(statusMessage);
}

void Client::startThreads() {
    mStopThreads = false;
    mRecvThread = std::thread(&Client::recvThreadFunc, this);
    mSendThread = std::thread(&Client::sendThreadFunc, this);
}

void Client::stopThreads() {
    mStopThreads = true;
    mRecvMsgCondVar.notify_all();
    mSendMsgCondVar.notify_all();
    if (mRecvThread.joinable()) {
        mRecvThread.join();
    }
    if (mSendThread.joinable()) {
        mSendThread.join();
    }
}

void Client::recvThreadFunc() {
    while (!mStopThreads) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        recvMsg();
    }
}

void Client::sendThreadFunc() {
    while (!mStopThreads) {
        {
            std::unique_lock<std::mutex> lock(mSendMsgMutex);
            mSendMsgCondVar.wait(lock, [this] { return !mSendMsgQueue.empty() || mStopThreads; });
        }

        if (!mStopThreads) {
            if (sendMsg() == ClientError::Failed) {
                std::cerr << "Failed to send message from queue" << std::endl;
            }
        }
    }
}

} // namespace chat_app
