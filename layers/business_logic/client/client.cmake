cmake_minimum_required(VERSION 3.22)
set(CMAKE_CXX_STANDARD 17)
set(LIB_NAME "client")

# project(${LIB_NAME})

set(SRC_DIR
    ${CMAKE_CURRENT_LIST_DIR}/src/client.cpp
   )

set(INCLUDE_DIR
    ${CMAKE_CURRENT_LIST_DIR}/include
   )

add_library(${LIB_NAME} STATIC)
target_sources(${LIB_NAME} PUBLIC ${SRC_DIR})
target_include_directories(${LIB_NAME} PUBLIC ${INCLUDE_DIR})
target_link_libraries(ChatAppBll INTERFACE ${LIB_NAME})