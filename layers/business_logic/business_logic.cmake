add_library(ChatAppBll INTERFACE)

include(${CMAKE_CURRENT_LIST_DIR}/facade/facade.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/server/server.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/client/client.cmake)