cmake_minimum_required(VERSION 3.22)
set(CMAKE_CXX_STANDARD 17)
set(LIB_NAME "facade")

# project(${LIB_NAME})

set(SRC_DIR
    ${CMAKE_CURRENT_LIST_DIR}/src/chat_app_facade.cpp
    ${CMAKE_CURRENT_LIST_DIR}/src/chat_app_factory.cpp
   )

set(INCLUDE_DIR
    ${CMAKE_CURRENT_LIST_DIR}/include
   )

add_library(${LIB_NAME} STATIC)
target_sources(${LIB_NAME} PUBLIC ${SRC_DIR})
target_include_directories(${LIB_NAME} PUBLIC ${INCLUDE_DIR})
target_link_libraries(ChatAppBll INTERFACE ${LIB_NAME})
add_dependencies(${LIB_NAME} server) 
add_dependencies(${LIB_NAME} client) 
target_link_libraries(${LIB_NAME} PUBLIC server)
target_link_libraries(${LIB_NAME} PUBLIC client)



