#include "chat_app_factory.hpp"
#include "server.hpp"
#include "client.hpp"

namespace chat_app { 

std::shared_ptr<IServer> ChatAppFactory::createServer(int aPort) {
    return Server::getInstance(aPort);
}

std::shared_ptr<IClient> ChatAppFactory::createClient(int aPort, const std::string &aServerIp) {
    return std::make_shared<Client>(aServerIp, aPort);
}

} // namespace chat_app
