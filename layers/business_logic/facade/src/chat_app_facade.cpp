#include "chat_app_facade.hpp"
#include "chat_app_factory.hpp"
#include "server.hpp"
#include "client.hpp"
#include <iostream>
#include <thread>

namespace chat_app {

class ChatAppFacade::Impl {
public:
    Impl(const ChatAppType &aAppType, int aPort, const std::string &aServerIp)
        : mFactory(std::make_shared<ChatAppFactory>()), 
        mAppType(aAppType),
        mPort(aPort),
        mServerIp(aServerIp) {

        if(mAppType == ChatAppType::Server) {
            mServer = mFactory->createServer(aPort);
            mClient = nullptr;
        }
        else if (mAppType == ChatAppType::Client)
        {
            mClient = mFactory->createClient(aPort, aServerIp);
            mServer = nullptr;
        }
    }

    ChatAppError startApp() {
        if(mServer && mAppType == ChatAppType::Server) {
            mServer->startServer();
            return ChatAppError::None;
        }
        else if (mClient && mAppType == ChatAppType::Client)
        {
            mClient->startClient();
            return ChatAppError::None;
        }
        return ChatAppError::InvalidType;
    }

    ChatAppError stopApp() {
        if(mServer && mAppType == ChatAppType::Server) {
            mServer->stopServer();
            return ChatAppError::None;
        }
        else if (mClient && mAppType == ChatAppType::Client)
        {
            mClient->stopClient();
            return ChatAppError::None;
        }
        return ChatAppError::InvalidType;
    }

    ChatAppError connectApp() {
        if(mServer && mAppType == ChatAppType::Server) {
            mServer->connectToClient();
            return ChatAppError::None;
        }
        else if (mClient && mAppType == ChatAppType::Client)
        {
            mClient->connectToServer();
            return ChatAppError::None;
        }
        return ChatAppError::InvalidType;
    }

    ChatAppError sendMsg(const std::string &aMsg) {
        if(mServer && mAppType == ChatAppType::Server) {
            mServer->sendMsgToQueue(aMsg);
            return ChatAppError::None;
        }
        else if (mClient && mAppType == ChatAppType::Client)
        {
            mClient->sendMsgToQueue(aMsg);
            return ChatAppError::None;
        }
        return ChatAppError::InvalidType;
    }

    ChatAppError recvMsg(std::string &aRcvMsg) {
        if(mServer && mAppType == ChatAppType::Server) {
            ServerError error = mServer->recvMsgFromQueue(aRcvMsg);
            if(error == ServerError::None) {
                return ChatAppError::None;
            }
            return ChatAppError::Failed;
        }
        else if (mClient && mAppType == ChatAppType::Client)
        {
            ClientError error = mClient->recvMsgFromQueue(aRcvMsg); 
            if(error == ClientError::None) {
                return ChatAppError::None;
            }
            return ChatAppError::Failed;         
        }
        return ChatAppError::InvalidType;
    }

    ChatAppStatus getAppStatus() {
        if (mServer && mAppType == ChatAppType::Server) {
            switch (mServer->getStatus()) {
                case ServerStatus::Online:
                    return ChatAppStatus::Online;
                case ServerStatus::Connected:
                    return ChatAppStatus::Connected;
                default:
                    return ChatAppStatus::Offline;
            }
        } 
        else if (mClient && mAppType == ChatAppType::Client) {
            switch (mClient->getStatus()) {
                case ClientStatus::Online:
                    return ChatAppStatus::Online;
                case ClientStatus::Connected:
                    return ChatAppStatus::Connected;
                default:
                    return ChatAppStatus::Offline;
            }
        }
        return ChatAppStatus::InvalidType;
    }

    ChatAppType getAppType() {
        return mAppType;     
    }

    std::string getIpAddr() {
        return mServerIp;     
    }

    int getPortNo() {
        return mPort;     
    }

private:
    std::shared_ptr<IChatAppFactory> mFactory;
    std::shared_ptr<IServer> mServer;
    std::shared_ptr<IClient> mClient;
    ChatAppType mAppType;
    int mPort;
    std::string mServerIp;
};

ChatAppFacade::ChatAppFacade(const ChatAppType &aAppType, int aPort, const std::string &aServerIp)
    : mImpl(std::make_unique<Impl>(aAppType, aPort, aServerIp)) {}

ChatAppFacade::~ChatAppFacade() = default;

ChatAppError ChatAppFacade::startApp() {
    return mImpl->startApp();
}

ChatAppError ChatAppFacade::stopApp() {
    return mImpl->stopApp();
}

ChatAppError ChatAppFacade::connectApp() {
    return mImpl->connectApp();
}

ChatAppError ChatAppFacade::sendMsg(const std::string &aMsg) {
    return mImpl->sendMsg(aMsg);
}

ChatAppError ChatAppFacade::recvMsg(std::string &aRcvMsg) {
    return mImpl->recvMsg(aRcvMsg);
}

ChatAppStatus ChatAppFacade::getAppStatus() const {
    return mImpl->getAppStatus();
}

ChatAppType ChatAppFacade::getAppType() const {
    return mImpl->getAppType();
}

std::string ChatAppFacade::getIpAddr() const {
    return mImpl->getIpAddr();
}

int ChatAppFacade::getPortNo() const {
    return mImpl->getPortNo();
}

} // namespace chat_app
