#ifndef CHAT_APP_FACTORY_INTERFACE_HPP
#define CHAT_APP_FACTORY_INTERFACE_HPP

#include "server_interface.hpp"
#include "client_interface.hpp"
#include <memory>

namespace chat_app { 

class IChatAppFactory {
public:
    virtual std::shared_ptr<IServer> createServer(int aPort) = 0;
    virtual std::shared_ptr<IClient> createClient(int aPort, const std::string &aServerIp) = 0;
    virtual ~IChatAppFactory() = default;
};

} // namespace chat_app

#endif // CHAT_APP_FACTORY_INTERFACE_HPP
