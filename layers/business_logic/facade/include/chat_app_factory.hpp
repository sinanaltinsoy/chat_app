#ifndef CHAT_APP_FACTORY_HPP
#define CHAT_APP_FACTORY_HPP

#include "chat_app_factory_interface.hpp"

namespace chat_app { 

class ChatAppFactory : public IChatAppFactory {
public:
    std::shared_ptr<IServer> createServer(int aPort) override;
    std::shared_ptr<IClient> createClient(int aPort, const std::string &aServerIp) override;
};

} // namespace chat_app

#endif // CHAT_APP_FACTORY_HPP
