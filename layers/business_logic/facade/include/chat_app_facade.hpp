#ifndef CHAT_APP_FACADE_HPP
#define CHAT_APP_FACADE_HPP

#include "chat_app_factory_interface.hpp"
#include "chat_app_types.hpp"
#include <memory>
#include <string>

namespace chat_app { 
    
class ChatAppFacade {
public: 
    ChatAppFacade(const ChatAppType &aAppType, int aPort, const std::string &aServerIp = ""); 
    ~ChatAppFacade();

    ChatAppError startApp();  
    ChatAppError stopApp(); 
    ChatAppError connectApp();  
    ChatAppError sendMsg(const std::string &aMsg); 
    ChatAppError recvMsg(std::string &aRcvMsg);
    ChatAppStatus getAppStatus() const;
    ChatAppType getAppType() const;
    std::string getIpAddr() const;
    int getPortNo() const;

private:
    class Impl;
    std::unique_ptr<Impl> mImpl;
};

} // namespace chat_app

#endif // CHAT_APP_FACADE_HPP
