#ifndef CHAT_APP_TYPES_HPP
#define CHAT_APP_TYPES_HPP

namespace chat_app {

enum class ChatAppStatus : unsigned char {
    Online,
    Offline,
    Connected,
    InvalidType
};

enum class ChatAppError : unsigned char {
    None = 0,
    NotConnected,
    InvalidType,
    Failed
};

enum class ChatAppType : unsigned char {
    Server,
    Client
};

}

#endif // CHAT_APP_TYPES_HPP
