#include "server.hpp"
#include <iostream>
#include <unistd.h>
#include <netinet/in.h>
#include <cstring>
#include <sys/socket.h>
#include <arpa/inet.h>

namespace chat_app {

std::shared_ptr<Server> Server::mInstance = nullptr;
std::mutex Server::mInstanceMutex;

std::shared_ptr<Server> Server::getInstance(int aPort) {
    std::lock_guard<std::mutex> lock(mInstanceMutex);
    if (mInstance == nullptr) {
        mInstance = std::shared_ptr<Server>(new Server(aPort));
    }
    return mInstance;
}

Server::Server(int aPort) : mPort(aPort), mServerSocket(-1), mClientSocket(-1), mServerStatus(ServerStatus::Offline) {
    startThreads();
}

Server::~Server() {
    if (mServerStatus == ServerStatus::Online) {
        stopServer();
    }
    stopThreads();
}

ServerError Server::startServer() {
    if (mServerStatus == ServerStatus::Offline) {
        mServerSocket = socket(AF_INET, SOCK_STREAM, 0);
        if (mServerSocket == -1) {
            std::cerr << "Failed to create socket" << std::endl;
            return ServerError::Failed;
        }

        sockaddr_in serverAddr;
        std::memset(&serverAddr, 0, sizeof(serverAddr));
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_addr.s_addr = INADDR_ANY;
        serverAddr.sin_port = htons(mPort);

        if (bind(mServerSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
            std::cerr << "Failed to bind socket" << std::endl;
            return ServerError::Failed;
        }

        if (listen(mServerSocket, 5) == -1) {
            std::cerr << "Failed to listen on socket" << std::endl;
            return ServerError::Failed;
        }

        setStatus(ServerStatus::Online);

        std::cout << "Server started on port " << mPort << std::endl;
        return ServerError::None;
    }
    return ServerError::Failed;
}

ServerError Server::stopServer() {
    if (mServerStatus != ServerStatus::Offline) {
        setStatus(ServerStatus::Offline);
        if (mServerSocket != -1) {
            close(mServerSocket);
            mServerSocket = -1;
        }
        if (mClientSocket != -1) {
            close(mClientSocket);
            mClientSocket = -1;
        }
        std::cout << "Server stopped" << std::endl;
    }
    return ServerError::None;
}

ServerError Server::connectToClient() {
    if(mServerStatus == ServerStatus::Online) {
        mClientSocket = accept(mServerSocket, nullptr, nullptr);
        if (mClientSocket == -1) {
            std::cerr << "Failed to accept client" << std::endl;
            return ServerError::Failed;
        }

        setStatus(ServerStatus::Connected);
        return ServerError::None;
    }
    return ServerError::Failed;
}

ServerError Server::sendMsg() {
    std::string aMsg;
    {
        std::lock_guard<std::mutex> lock(mSendMsgMutex);
        if (mSendMsgQueue.empty()) {
            return ServerError::Failed;
        }
        aMsg = mSendMsgQueue.front();
        mSendMsgQueue.pop();
    }

    if (mServerStatus == ServerStatus::Connected) {
        int retVal = send(mClientSocket, aMsg.c_str(), aMsg.length(), 0);
        if (retVal > 0) {
            std::cout << "Sent message: " << aMsg << std::endl;
            return ServerError::None;
        }
    }
    std::cerr << "Failed to send message" << std::endl;
    return ServerError::NotConnected;
}

ServerError Server::recvMsg() {
    if (mServerStatus == ServerStatus::Connected) {
        char buffer[1024];
        ssize_t bytesRead = recv(mClientSocket, buffer, sizeof(buffer) - 1, 0);
        if (bytesRead > 0) {
            buffer[bytesRead] = '\0';
            std::string message(buffer);
            {
                std::lock_guard<std::mutex> lock(mRcvMsgMutex);
                mRecvMsgQueue.push(message);
            }
            std::cout << "Received message: " << message << std::endl;
            mRecvMsgCondVar.notify_one();

            // Check for connection status change messages
            if (message == "Client is Offline") {
                setStatus(ServerStatus::Online);
            } else if (message == "Client is Online") {
                setStatus(ServerStatus::Online);
            }

            return ServerError::None;
        }
    }
    std::cerr << "Failed to receive message" << std::endl;
    return ServerError::Failed;
}

ServerError Server::sendMsgToQueue(const std::string &aMessage) {
    {
        std::lock_guard<std::mutex> lock(mSendMsgMutex);
        mSendMsgQueue.push(aMessage);
    }
    mSendMsgCondVar.notify_one();
    return ServerError::None;
}

ServerError Server::recvMsgFromQueue(std::string &aMessage) {
    std::lock_guard<std::mutex> lock(mRcvMsgMutex);
    if (!mRecvMsgQueue.empty()) {
        aMessage = mRecvMsgQueue.front();
        mRecvMsgQueue.pop();
        return ServerError::None;
    }
    return ServerError::Failed;
}

ServerStatus Server::getStatus() const {
    return mServerStatus;
}

void Server::setStatus(ServerStatus aStatus) {
    mServerStatus = aStatus;
    notifyClient();
}

void Server::notifyClient() {
    std::string statusMessage = "Server is ";
    if(mServerStatus == ServerStatus::Online) statusMessage += "Online";
    else if (mServerStatus == ServerStatus::Offline) statusMessage += "Offline";
    else if (mServerStatus == ServerStatus::Connected) statusMessage += "Connected";
    statusMessage += "\n";

    sendMsgToQueue(statusMessage);
}

void Server::startThreads() {
    mStopThreads = false;
    mRecvThread = std::thread(&Server::recvThreadFunc, this);
    mSendThread = std::thread(&Server::sendThreadFunc, this);
}

void Server::stopThreads() {
    mStopThreads = true;
    mRecvMsgCondVar.notify_all();
    mSendMsgCondVar.notify_all();
    if (mRecvThread.joinable()) {
        mRecvThread.join();
    }
    if (mSendThread.joinable()) {
        mSendThread.join();
    }
}

void Server::recvThreadFunc() {
    while (!mStopThreads) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        recvMsg();
    }
}

void Server::sendThreadFunc() {
    while (!mStopThreads) {
        {
            std::unique_lock<std::mutex> lock(mSendMsgMutex);
            mSendMsgCondVar.wait(lock, [this] { return !mSendMsgQueue.empty() || mStopThreads; });
        }

        if (!mStopThreads) {
            sendMsg();
        }
    }
}

} // namespace chat_app
