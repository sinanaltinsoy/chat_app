#ifndef SERVER_INTERFACE_HPP
#define SERVER_INTERFACE_HPP

#include "server_types.hpp"

namespace chat_app { 

class IServer {
public:
    virtual ServerError startServer() = 0;
    virtual ServerError stopServer() = 0;
    virtual ServerError recvMsgFromQueue(std::string &aMsg) = 0;
    virtual ServerError recvMsg() = 0;
    virtual ServerError sendMsgToQueue(const std::string &aMsg) = 0;
    virtual ServerError sendMsg() = 0;
    virtual ServerError connectToClient() = 0;
    virtual void setStatus(ServerStatus aStatus)  = 0;
    virtual ServerStatus getStatus() const  = 0;
    virtual void notifyClient() = 0;
    virtual void startThreads() = 0;
    virtual void stopThreads() = 0;
    virtual void recvThreadFunc() = 0;
    virtual void sendThreadFunc() = 0;
    virtual ~IServer() = default;
};

} // namespace chat_app

#endif // SERVER_INTERFACE_HPP
