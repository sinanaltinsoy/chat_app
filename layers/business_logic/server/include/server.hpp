#ifndef SERVER_HPP
#define SERVER_HPP

#include "server_interface.hpp"
#include "server_types.hpp"
#include <memory>
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <queue>
#include <string>

namespace chat_app {

class Server : public IServer {
public:
    static std::shared_ptr<Server> getInstance(int aPort);

    ServerError startServer() override;
    ServerError stopServer() override;
    ServerError recvMsgFromQueue(std::string &aMsg) override;
    ServerError recvMsg() override;
    ServerError sendMsgToQueue(const std::string &aMsg) override;
    ServerError sendMsg() override;
    ServerError connectToClient() override;
    ServerStatus getStatus() const override;
    void setStatus(ServerStatus aStatus) override;
    void notifyClient() override;
    void startThreads() override;
    void stopThreads() override;
    void recvThreadFunc() override;
    void sendThreadFunc() override;
    ~Server();

private:
    Server(int aPort);
    ServerStatus mServerStatus;
    int mClientSocket;
    int mServerSocket;
    int mPort;
    std::queue<std::string> mRecvMsgQueue;
    std::queue<std::string> mSendMsgQueue;
    std::mutex mRcvMsgMutex;
    std::mutex mSendMsgMutex;
    std::condition_variable mRecvMsgCondVar;
    std::condition_variable mSendMsgCondVar;
    std::thread mRecvThread;
    std::thread mSendThread;
    std::atomic<bool> mStopThreads{false};
    static std::shared_ptr<Server> mInstance;
    static std::mutex mInstanceMutex;
};

} // namespace chat_app 

#endif // SERVER_HPP
