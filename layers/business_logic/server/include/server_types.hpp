#ifndef SERVER_TYPES_HPP
#define SERVER_TYPES_HPP

#include <string>

namespace chat_app {

    enum class ServerStatus : unsigned char  {
        Online,
        Offline,
        Connected,
    };

    enum class ServerError : unsigned char {
        None = 0,
        NotConnected,
        InvalidType,
        Failed
    };

}

#endif // SERVER_TYPES_HPP
