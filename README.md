
# CHAT_APP Documentation

## Table of Contents
1. [Introduction](#introduction)
2. [Dependencies](#dependencies)
3. [Setup and Building](#setup-and-building)
4. [Application Design and Architecture](#application-design-and-architecture)
5. [Application Interfaces](#application-interfaces)
6. [UML Diagram](#uml-diagram)

## Introduction
CHAT_APP is a simple server-client communication application. This application allows unicast messaging between server and client. The application is designed using various software design patterns to manage the interaction between the user interface and the backend processes efficiently.

**Note:** To keep it simple, the server accepts only one client.

## Dependencies
To compile and run the application, the following dependencies are required:
1. **CMake 22 or higher:** CMake 22 or a later version must be installed to build the project.
2. **Qt 6.7:** Qt 6.7 is used for the graphical user interface. You can build Qt 6.7 from the following link: [Building Qt 6 from Git](https://wiki.qt.io/Building_Qt_6_from_Git).
   - **Note:** Since the latest versions of Qt do not include certain font supports, additional font support should be added.

## Setup and Building
After installing the required dependencies, follow these steps to build the project on Ubuntu 22.04:

### Environment Variables
First, set up the following environment variables:
```
APP_NAME="CHAT_APP"
ENV_PATH="${workspaceFolder}/cmake"
QT_PATH="/home/user/qt6-build"
APP_PATH_UBUNTU="${workspaceFolder}/_build"
BUILD_PATH_UBUNTU="${workspaceFolder}/_build"
SOURCE_PATH_UBUNTU="${workspaceFolder}/source"
```

### CMake Commands
Run the following commands to configure and build the project:
```
cd $SOURCE_PATH_UBUNTU
cmake -D CMAKE_PREFIX_PATH=$QT_PATH -C $ENV_PATH/env.cmake -S $SOURCE_PATH_UBUNTU -B $BUILD_PATH_UBUNTU -G Ninja
ninja -C $BUILD_PATH_UBUNTU
```

### Running the Application
After the build process is completed, you can run the application with the following command:
```
$BUILD_PATH_UBUNTU/$APP_NAME
```

## Application Design and Architecture
CHAT_APP consists of two layers:

1. **Business Logic Layer:**
   - This layer contains the application’s business logic and data processing functions. Communication between the server and the client is managed in this layer.
   
2. **User Interface Logic Layer:**
   - This layer manages the user interface and user interactions. The user interface is designed with Qt.

Both layers are compiled as separate libraries and invoked by \`main.cpp\`. This structure ensures that the application is more modular and manageable.

### Software Design Patterns Used

#### PIMPL (Pointer to Implementation)
- **Purpose:** To hide the libraries used by the interface and make the application more modular and manageable.
- **Usage:** Separates the interface from the implementation details, reducing compile times and hiding dependencies.

#### Facade Pattern
- **Purpose:** To simplify the interaction between the user interface and the underlying complex system.
- **Usage:** Manages complex backend processes through a single interface, keeping the user interface clean and manageable.

#### Factory Pattern
- **Purpose:** To centrally manage the object creation process.
- **Usage:** Used to create client or server objects through the \`ChatApp\` class.

#### Singleton Pattern
- **Purpose:** To ensure only one instance of a class is created.
- **Usage:** Used when only one instance of the server should exist.

## UML Diagram
Below is the UML diagram showing the class structure of the application. This diagram illustrates the relationships between the classes and the methods each class contains.

![UML Diagram](docs/design.png)

## Graphical User Interfaces
The application has two main user interfaces:

### Main Window
The user can select server or client mode, and enter the IP address and port number.

![Main Window](docs/main_window.png)

### Chat Window
After establishing a connection, this window is used for messaging. Separate windows exist for the server and the client.

![Chat Window](docs/chat_window.png)