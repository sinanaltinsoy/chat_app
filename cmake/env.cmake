set(ROOT_PATH                   ${CMAKE_CURRENT_LIST_DIR}/..)
set(ROOT_PATH                   ${ROOT_PATH}                CACHE STRING "root path")
set(LAYERS_PATH                 ${ROOT_PATH}/layers         CACHE STRING "layers path")
set(CMAKE_FILES_PATH            ${ROOT_PATH}/cmake          CACHE STRING "cmake path")
set(CMAKE_CONFIG_SEARCH_DIR     ${ROOT_PATH}/cmake          CACHE STRING "configs path")

message("-- Root is ${ROOT_PATH}")