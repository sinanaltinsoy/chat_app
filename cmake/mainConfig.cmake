set(CMAKE_VERSION 3.22)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
find_package(Qt6 REQUIRED COMPONENTS Core Gui Widgets)
qt_standard_project_setup()

include(${ROOT_PATH}/layers/layers.cmake)

function(chat_app_add_executable)
    set(prefix "TARGET")
    set(options  "")
    set(oneValueArgs NAME)
    set(multiValueArgs SOURCES INCLUDES LIBRARIES COMPILE_DEFINITIONS DEPENDENCIES)

    cmake_parse_arguments("${prefix}"  "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if(NOT TARGET_SOURCES)
        message(FATAL_ERROR "NO SOURCE DETECTED!")
    endif()
    
    add_executable(${TARGET_NAME})

    target_sources(${TARGET_NAME} PRIVATE ${TARGET_SOURCES})

    target_include_directories(${TARGET_NAME} PRIVATE ${TARGET_INCLUDES})

    target_compile_definitions(${TARGET_NAME} PRIVATE ${TARGET_COMPILE_DEFINITIONS} )

    target_link_libraries(${TARGET_NAME} PRIVATE ${TARGET_LIBRARIES})
    add_dependencies(${TARGET_NAME} ${TARGET_LIBRARIES})  
    
endfunction()